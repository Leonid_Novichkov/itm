#include "ITM/itm.h"


void itm_init(void){
    //IOCPortConfigureSet(IOID_18, IOC_PORT_MCU_SWV, IOC_STD_OUTPUT);
         HWREG(CPU_SCS_BASE + CPU_SCS_O_DEMCR) |= CPU_SCS_DEMCR_TRCENA;
        // Give Access Control
        HWREG(CPU_ITM_BASE + CPU_ITM_O_LAR) = 0xC5ACCE55;         //unlock word
        HWREG(CPU_TPIU_BASE + CPU_TPIU_O_SPPR) = CPU_TPIU_SPPR_PROTOCOL_SWO_NRZ;

        // Define the speed on the wire.
        HWREG(CPU_TPIU_BASE + CPU_TPIU_O_ACPR) &= ~(0x1FFF);
        HWREG(CPU_TPIU_BASE + CPU_TPIU_O_ACPR) |= ((48000000 / (115200) - 1));

        HWREG(CPU_TPIU_BASE + CPU_TPIU_O_FFCR) = CPU_TPIU_FFCR_TRIGIN;

        // Control register: enable ITM
        HWREG(CPU_ITM_BASE + CPU_ITM_O_TCR) = CPU_ITM_TCR_ITMENA;

        // Control Register: Apply stimulus mask
        HWREG(CPU_ITM_BASE + CPU_ITM_O_TER) = CPU_ITM_TER_STIMENA0; // Enable just stimuli 0

        IOCPortConfigureSet(IOID_16, IOC_PORT_MCU_SWV, IOC_IOMODE_NORMAL);

}


