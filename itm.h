#ifndef ITM_ITM_H_
#define ITM_ITM_H_

#include <ti/devices/DeviceFamily.h>
#include DeviceFamily_constructPath(inc/hw_types.h)
#include DeviceFamily_constructPath(inc/hw_memmap.h)
#include DeviceFamily_constructPath(inc/hw_cpu_itm.h)
#include DeviceFamily_constructPath(inc/hw_cpu_scs.h)
#include DeviceFamily_constructPath(inc/hw_cpu_tpiu.h)
#include DeviceFamily_constructPath(driverlib/ioc.h)

void itm_init(void);

#endif /* ITM_ITM_H_ */
